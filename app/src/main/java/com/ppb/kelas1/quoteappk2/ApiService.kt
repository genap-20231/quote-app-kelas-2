package com.ppb.kelas1.quoteappk2

import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("random")
    fun getRandomQuote(): Call<QuoteResponse>
}