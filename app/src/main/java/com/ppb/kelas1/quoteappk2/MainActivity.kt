package com.ppb.kelas1.quoteappk2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    lateinit var tvQuote: TextView
    lateinit var tvAuthor: TextView
    lateinit var progressBar: ProgressBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        //Init variabel view
        tvAuthor = findViewById(R.id.tv_author)
        tvQuote = findViewById(R.id.tv_quote)
        progressBar = findViewById(R.id.progressBar)
        val clParent=findViewById<ConstraintLayout>(R.id.cl_parent)
        tvQuote.setOnClickListener {
            getDataFromApi()
        }
        tvAuthor.setOnClickListener {
            getDataFromApi()
        }
        clParent.setOnClickListener {
            getDataFromApi()
        }
        getDataFromApi()
    }

    fun getDataFromApi() {
        progressBar.visibility = View.VISIBLE
        val call = ApiClient.apiService.getRandomQuote();
        call.enqueue(object : Callback<QuoteResponse> {
            override fun onResponse(call: Call<QuoteResponse>, response: Response<QuoteResponse>) {
                if (response.isSuccessful) {
                    val quoteResponse = response.body()
                    tvQuote.text = quoteResponse?.quote
                    tvAuthor.text = quoteResponse?.author
                }
                progressBar.visibility = View.GONE
            }

            override fun onFailure(call: Call<QuoteResponse>, t: Throwable) {
            }
        })
    }
}