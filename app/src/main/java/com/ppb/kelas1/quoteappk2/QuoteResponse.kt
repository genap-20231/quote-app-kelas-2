package com.ppb.kelas1.quoteappk2

data class QuoteResponse(
    val author: String,
    val quote: String
)
